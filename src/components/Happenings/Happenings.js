import React, { useEffect, useState } from "react";
import { Paper, Typography } from "@mui/material";
import HappeningCard from "./HappeningCard";
import { theShack } from "../../js/bm";

export default function Happenings({ id }){
    const [happenings, setHappenings] = useState([]);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(async ()=> {
        setHappenings(await theShack.provenance.getHappening(id)
                .then(res => {
                    return res.data;
                }))
    }, [])
    return (
        <Paper className={"bm-d-happenings"}>
            <Typography className={"bm-d-happenings-header"} variant={"h4"}>Distinguished Happenings</Typography>
            {
                happenings.map((happening, i) => {
                    return (
                        <HappeningCard key={i} happening={happening} />
                    )
                })
            }
        </Paper>
    )
}
