import React from "react";
import { Avatar, Card, CardContent, CardHeader, Typography } from "@mui/material";

export default function HappeningCard({ happening }) {

    let courtesy = happening['PublicKey'].substring(0, 4) + "..." + happening['PublicKey'].substring((happening['PublicKey'].length - 5), happening['PublicKey'].length)
    let date = new Date(happening['CheckinDate']).toLocaleDateString('en-US', {year:'numeric', day: 'numeric', month: 'long'});

    return (
        <Card className={"bm-happening"}>
            <Avatar className={"bm-happening-avatar"} src={happening['ImageURL']}/>
            <CardContent className={"bm-happening-content"}>
                <CardHeader className={"bm-happening-header"} title={happening['Name']} subheader={date}/>
                <Typography className={"bm-happening-desc"} variant={"body2"}>{happening['Description']}</Typography>
                <Typography className={"bm-happening-footer"} variant={"body1"}>{"Courtesy of: 0x" + courtesy}</Typography>
            </CardContent>
        </Card>
    )
}
