import React from "react";
import {
    Avatar,
    Card,
    CardContent,
    CardHeader,
    CardMedia, Grid,
    List,
    ListItem,
    ListItemText,
    Typography
} from "@mui/material";

export default function TokenPanel({id}) {

    let date = new Date("August 15, 2022").toLocaleDateString('en-US', {year:'numeric', day: 'numeric', month: 'long'});

    return (
        <Card className={"bm-token-panel"}>
            <CardMedia className={"bm-token-image"} component={"img"} src={process.env.PUBLIC_URL + "/assets/images/43.png"}/>
            <CardContent className={"bm-token-container"}>
                <CardHeader className={"bm-token-header"} title={"Bill Murray"} subheader={"#" + id}/>
                <Typography className={"bm-token-owner"} variant={"h5"}>{"Owned By: " + "0xe4cA...EB22" + " since " + date}</Typography>
                <CardContent className={"bm-token-attr-content"}>
                    <CardHeader className={"bm-token-attr-header"} title={"Design Attributes"}/>
                    <Grid container className={"bm-token-attr-list"}>
                        <Grid item xs={6}>
                            <ListItemText className={"bm-token-attr"}>Vibe: Classic</ListItemText>
                        </Grid>
                        <Grid item xs={6}>
                            <ListItemText className={"bm-token-attr"}>Tie: Green</ListItemText>
                        </Grid>
                        <Grid item xs={6}>
                            <ListItemText className={"bm-token-attr"}>Glasses: Red/Blue</ListItemText>
                        </Grid>
                        <Grid item xs={6}>
                            <ListItemText className={"bm-token-attr"}>Jacket: Black</ListItemText>
                        </Grid>
                    </Grid>
                </CardContent>
            </CardContent>
        </Card>
    )

}
