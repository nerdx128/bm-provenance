import React from 'react';
import './styles/output.css';
import BMRoutes from "./BMRoutes";


function App() {
  return (
    <div className="App">
        <BMRoutes/>
    </div>
  );
}

export default App;
