import React from "react";
import { useParams } from "react-router";
import Happenings from "../components/Happenings/Happenings";
import TokenPanel from "../components/Panels/TokenPanel";
import {AppBar, TextField, Toolbar, Typography} from "@mui/material";
import { Autocomplete } from "@mui/material";

export default function Provenance(){
    let { id } = useParams();

    return (
        <div>
            <AppBar className={"bm-appbar"}>
                <Typography className={"bm-search-label"} variant={"h6"}>Looking for another Bill?</Typography>
                <Autocomplete
                    className={"bm-search-bar"}
                    freeSolo
                    id="free-solo-2-demo"
                    disableClearable
                    options={token_ids.map((token) => token.id)}
                    renderInput={(params) => (
                        <TextField
                            className={"bm-search-bar-output"}
                            {...params}
                            label="Search for token ID..."
                            InputProps={{
                                ...params.InputProps,
                                type: 'search',
                            }}
                        />
                    )}
                />
            </AppBar>
            <TokenPanel id={id}/>
            <Happenings id={id}/>
        </div>
    )
}

const token_ids = [
    { id: 16 },
    { id: 42 },
    { id: 43 },
    { id: 75 },
    { id: 115 },
    { id: 231 }
]
