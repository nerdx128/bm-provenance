import React from "react";
import { Routes, Route } from "react-router-dom";
import Provenance from "./pages/Provenance";

export default function MHDRoutes() {
    return (
        <Routes>
            <Route path="/" element = { <Provenance/> }/>
            <Route path="/Provenance/:id" element = { <Provenance/> }/>
        </Routes>
    );
}
