import axios from "axios";

let api = {
    host: "https://bmhappenings.azurewebsites.net"
}

export const theShack = {
    provenance: {
        getHappening: (id) => {
            let method = "/happenings";
            return axios.get(api.host + method + "?id=" + id)
                .then(async res => {
                    console.log(res)
                    return res;
                })
        }
    }
}
